﻿using System.Collections;
using UnityEngine;

public class Cube : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void Destroy(Vector3 targetAnimationPosition, float mergeAnimationTime)
    {
        StartCoroutine(DestroyObjectAnimation(targetAnimationPosition, mergeAnimationTime));
    }

    IEnumerator DestroyObjectAnimation(Vector3 targetAnimationPosition, float mergeAnimationTime)
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("start");
        yield return new WaitForSeconds(mergeAnimationTime * 0.6f);

        LeanTween.moveLocalZ(gameObject, targetAnimationPosition.z, mergeAnimationTime * 0.4f).setEaseOutBack();
        yield return new WaitForSeconds(mergeAnimationTime * 0.4f);
        Destroy(gameObject);
    }
}
