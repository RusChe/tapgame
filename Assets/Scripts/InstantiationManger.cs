﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InstantiationManger : MonoBehaviour
{
    [SerializeField]
    bool allowInstantiationDuringMerg = true;
    [SerializeField]
    float mergeAnimationTime;
    [SerializeField]
    float Offset;
    [SerializeField]
    GameObject[] objectPrefabs;
    List<List<GameObject>> Instantiated = new List<List<GameObject>>();

    bool merging = false;
    int count = 0;
    int merges = 0;
    List<int> CounterDigits;

    public int Taps { get => count;}
    public int Merges { get => merges; }

    public void RunTest()
    {
        StartCoroutine(Click());
    }

    private void Start()
    {
        foreach (var item in objectPrefabs)
        {
            Instantiated.Add(new List<GameObject>());
        }
    }

    IEnumerator Click()
    {
        while (count < Mathf.Pow(10, objectPrefabs.Length))
        {
            InstantiateNext();
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    int GetTypeIndex()
    {
        return CounterDigits.Count;
    }
    int GetBasicPositionIndex()
    {
        return CounterDigits.Sum();
    }

    void DecopleCounter()
    {
        var digits = new List<int>();
        var integer = count;
        while (integer > 0)
        {
            digits.Add(integer % 10);
            integer /= 10;
        }
        CounterDigits = digits;
    }

    Vector3 GetTargetPosition()
    {
        return new Vector3(transform.position.x,
                           transform.position.y,
                           transform.position.z + (GetBasicPositionIndex() - 1) * Offset);
    }

    bool IsAllowed()
    {
        if (allowInstantiationDuringMerg)
            return true;
        else
            if (merging)
            return false;
        else
            return true;

    }

    public void InstantiateNext()
    {
        if (IsAllowed())
        {
            AnalyticsEventManager.Tap();
            count++;
            DecopleCounter();
            Instantiated[0].Add(Instantiate(objectPrefabs[0], GetTargetPosition(), transform.rotation, transform));
            TryMerge();
        }
    }

    void TryMerge()
    {
        int typeIndex = 0;
        for (int i = 0; i < CounterDigits.Count - 1; i++)
        {
            if (CounterDigits[i] != 0)
                break;
            typeIndex++;
        }

        if (typeIndex != 0)
        {
            merging = true;
            merges++;
            AnalyticsEventManager.Merge();

            for (int j = 0; j < typeIndex; j++)
            {
                StartCoroutine(MergeType(Instantiated[j], typeIndex));
                Instantiated[j] = new List<GameObject>();
            }
        }
    }

    IEnumerator MergeType(List<GameObject> toMerge, int typeIndex)
    {
        Vector3 targetPosition = GetTargetPosition();
        for (int i = 0; i < toMerge.Count; i++)
        {
            toMerge[i].GetComponent<Cube>().Destroy(targetPosition, mergeAnimationTime);
        }
        yield return new WaitForSeconds(mergeAnimationTime);
        Instantiated[typeIndex].Add(Instantiate(objectPrefabs[typeIndex], targetPosition, transform.rotation, transform));
        merging = false;
    }

}
