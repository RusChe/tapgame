﻿using UnityEngine;
using UnityEngine.UI;

public class GameCanvas : MonoBehaviour
{
    [SerializeField]
    Text Taps;
    [SerializeField]
    Text Merges;

    InstantiationManger instantiationManger;
    private void Start()
    {
        instantiationManger = FindObjectOfType<InstantiationManger>();
    }

    void FixedUpdate()
    {
        Taps.text = $"Taps:{instantiationManger.Taps.ToString()}";
        Merges.text = $"Merges:{instantiationManger.Merges.ToString()}";
    }
}
