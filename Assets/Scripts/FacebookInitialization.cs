﻿using Facebook.Unity;
using UnityEngine;
public class FacebookInitialization : MonoBehaviour
{
    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                    FB.ActivateApp();
                else
                    Debug.LogError("Couldnt initialize");
            });
        }
        else
            FB.ActivateApp();
    }
}
