﻿using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField]
    Canvas MainCanvas;
    [SerializeField]
    Canvas PauseCanvas;
    [SerializeField]
    Canvas GameCanvas;
    [SerializeField]
    Canvas StartingCanvas;


    private void Start()
    {
        StartingCanvas.enabled = true;
        PauseCanvas.enabled = false;
        MainCanvas.enabled = false;
        GameCanvas.enabled = false;
    }
    public void PauseGame()
    {
        AnalyticsEventManager.Pause();
        StartingCanvas.enabled = false;
        PauseCanvas.enabled = true;
        MainCanvas.enabled = false;
        GameCanvas.enabled = false;
    }

    public void ResumeGame()
    {
        AnalyticsEventManager.TapToPlay();
        StartingCanvas.enabled = false;
        PauseCanvas.enabled = false;
        MainCanvas.enabled = true;
        GameCanvas.enabled = true;
    }
    public void ExitGame()
    {
        Application.Quit();
    }

}
