﻿using GameAnalyticsSDK;
using UnityEngine;
public class AnalyticsEventManager : MonoBehaviour
{

    private void Start()
    {
        GameAnalytics.Initialize();
    }

    public static void Tap()
    {
        GameAnalytics.NewDesignEvent("Tap", 1);
    }

    public static void Merge()
    {
        GameAnalytics.NewDesignEvent("Merge", 1);
    }

    public static void TapToPlay()
    {
        GameAnalytics.NewDesignEvent("TapToPlay", 1);

    }

    public static void Pause()
    {
        GameAnalytics.NewDesignEvent("Pause", 1);

    }
}
